# Test your might

The goal of this project is to present a number of scenarios to help improve basic Systems Administration and Command Line skills to learn by doing.

The idea came from break/fix scenarios that I have been put through duing interviews. I enjoy practical application of knowledge vs pure theory and have always had this dream of having access to a repository of broken VMs that you can use to test your own troubleshooting methodology against (similar to HTB or Wargames in InfoSec). This will serve as an alternative option until someone smarter than me comes along and makes my dream a reality. 

## general OS level stuff

## website
- Set up a basic HTML website on a Linux server.
- Set up a Wordpress site or other CMS.
  - Backup the website fromt the CLI.
  - Migrate the website to another Linux instance (can use another cloud provider if you want).
- Install a self-signed SSL certificate.
  - (Optional) - Set up a Load Balancer instance, install the SSL certificate on the LB.
  - Test connections to the website and observe the web server logs.
  - Configure LB to show the client/visitor original IP address in the logs.

## database
- Install MySQL/MariaDB on a Linux server using the secure installation method.
  - Configure the MySQL server to start on boot.
  - Create a non-root user and apply appropriate grants for administrative access.
- Run some basic MySQL queries from inside the MySQL shell as well as from the server command line (mysql command).
  - List all the databases.
  - List all the users and their grants.
  - List tables.
  - Use select statements to pull data from tables.
- Import/Export a MySQL database using mysqldump command (Create your own or research open source databases for test purposes).
  - For larger databases, use compression to export the sql file.
  - Try the import/export actions on specific databases while excluding others.
- Implement an alternative backup solution for your database (Look into Percona XtraBackup, Holland).
  - Set up scheduled backups.
  - Validate backup jobs.
  - Restore backups to ensure backup integrity.
- Set up Replication.
  - Configure MHA for Highly Available Replication group.

## scripting
- Create a script that displays all the users on a Linux server.
  - The output should show the username and the corresponding home directory, seprated by a colon.
- Create a script that monitors login attempts and stores them in a separate log file.
  - Schedule a cron job to run this script once a day.
- Write a script that benchmarks block storage volumes.
  - Have the benchmark output to a results file that you can view after the script runs.

## misc
- Create and configure an LDAP server.

